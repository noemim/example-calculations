from django.conf.urls import include, url
from testing.tests import AddTwoView

urlpatterns = [
    url(r'^add-two/$', AddTwoView.as_view(), name='testing-add_two'),
]
