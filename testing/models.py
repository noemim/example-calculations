from calculations.models import BaseCalculationCache
from django.core.validators import MinValueValidator
from django.db import models

class AddTwo(BaseCalculationCache):
    result_field = 'sum'

    # Note: SQLite doesn't validate mins for positive integers, hence the
    # explicit validators
    first = models.PositiveIntegerField(validators=[MinValueValidator(1),])
    second = models.PositiveIntegerField(validators=[MinValueValidator(1),])
    sum = models.BigIntegerField(blank=True)

    def calculate_result(self):
        self.sum = self.first + self.second

    class Meta:
        unique_together = ['first', 'second']
