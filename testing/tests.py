import datetime
import pytz
import simplejson
from calculations.models import SquareDiffs, Triplet
from calculations.services import BaseCalculationService, SquareDiffService, \
                                  TripletService
from calculations.views import BaseCalculationView
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.utils import override_settings
from freezegun import freeze_time
from testing.models import AddTwo


class AddTwoService(BaseCalculationService):
    model = AddTwo
    lookup_keys = ['first', 'second']


class AddTwoView(BaseCalculationView):
    calculation_service = AddTwoService
    input_keys = ['first', 'second']


class BaseCalculationCacheTest(TestCase):
    # @TODO test misconfigured cases
    def test_run(self):
        adder = AddTwo(first=1, second=2)
        adder.save()
        self.assertEqual(3, adder.sum)
        self.assertEqual(0, adder.occurrences)
        self.assertEqual(1, AddTwo.objects.count())

        adder.run()
        self.assertEqual(3, adder.sum)
        self.assertEqual(1, adder.occurrences)
        self.assertEqual(1, AddTwo.objects.count())

        adder.run()
        self.assertEqual(3, adder.sum)
        self.assertEqual(2, adder.occurrences)
        self.assertEqual(1, AddTwo.objects.count())


class SquareDiffsTestCase(TestCase):
    def test_calculate(self):
        sqd = SquareDiffs(number=1)
        sqd.calculate_result()
        self.assertEqual(0, sqd.value)

        sqd = SquareDiffs(number=2)
        sqd.calculate_result()
        self.assertEqual(4, sqd.value)

        sqd = SquareDiffs(number=10)
        sqd.calculate_result()
        self.assertEqual(2640, sqd.value)

        sqd = SquareDiffs(number=100)
        sqd.calculate_result()
        self.assertEqual(25164150, sqd.value)

    def test_run(self):
        sqd = SquareDiffs(number=10)
        sqd.save()
        self.assertEqual(2640, sqd.value)
        self.assertEqual(0, sqd.occurrences)

        sqd.run()
        self.assertEqual(2640, sqd.value)
        self.assertEqual(1, sqd.occurrences)

        sqd.run()
        self.assertEqual(2640, sqd.value)
        self.assertEqual(2, sqd.occurrences)


class TripletsTestCase(TestCase):
    # @TODO: a few more edge cases
    def test_calculate(self):
        trip = Triplet(a=1, b=2, c=3)
        trip.calculate_result()
        self.assertEqual(6, trip.product)
        self.assertEqual(False, trip.is_pythagorean)

        trip = Triplet(a=3, b=4, c=5)
        trip.calculate_result()
        self.assertEqual(60, trip.product)
        self.assertEqual(True, trip.is_pythagorean)

    def test_run(self):
        trip = Triplet(a=1, b=2, c=3)
        trip.save()
        self.assertEqual(6, trip.product)
        self.assertEqual(False, trip.is_pythagorean)
        self.assertEqual(0, trip.occurrences)

        trip.run()
        self.assertEqual(1, trip.occurrences)
        trip.run()
        self.assertEqual(2, trip.occurrences)



class BaseCalculationServiceTest(TestCase):
    # @TODO test misconfigured cases
    def test_get_model_class(self):
        service = AddTwoService({})
        self.assertEqual(AddTwo, service.get_model_class())

    def test_get_lookup_keys(self):
        service = AddTwoService({})
        self.assertItemsEqual(['first', 'second'], service.get_lookup_keys())

    def test_get_lookup(self):
        service = AddTwoService([1, 2])
        self.assertEqual({ 'first': 1, 'second': 2 }, service.get_lookup())

        service = AddTwoService([2, 1])
        self.assertEqual({ 'first': 1, 'second': 2 }, service.get_lookup())

        service = AddTwoService([1, 2, 3])
        with self.assertRaises(AddTwoService.LookupError):
            service.get_lookup()

    def test_get_instance(self):
        self.assertEqual(0, AddTwo.objects.count())
        service = AddTwoService([1, 2])

        instance = service.get_instance()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(instance.sum, 3)
        self.assertEqual(instance.occurrences, 0)

        instance = service.get_instance()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(instance.sum, 3)
        self.assertEqual(instance.occurrences, 0)

        service = AddTwoService([2, 1])
        instance = service.get_instance()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(instance.sum, 3)
        self.assertEqual(instance.occurrences, 0)

        service = AddTwoService([3, 2])
        instance = service.get_instance()
        self.assertEqual(2, AddTwo.objects.count())
        self.assertEqual(instance.sum, 5)
        self.assertEqual(instance.occurrences, 0)

        service = AddTwoService([-1, 2])
        with self.assertRaises(AddTwoService.InputError):
            instance = service.get_instance()
        self.assertEqual(2, AddTwo.objects.count())

    def test_get_calculation(self):
        self.assertEqual(0, AddTwo.objects.count())
        service = AddTwoService([1, 2])

        result = service.get_calculation()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(result['sum'], 3)
        self.assertEqual(result['occurrences'], 1)

        result = service.get_calculation()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(result['sum'], 3)
        self.assertEqual(result['occurrences'], 2)

        service = AddTwoService([2, 1])
        result = service.get_calculation()
        self.assertEqual(1, AddTwo.objects.count())
        self.assertEqual(result['sum'], 3)
        self.assertEqual(result['occurrences'], 3)

        service = AddTwoService([3, 2])
        result = service.get_calculation()
        self.assertEqual(2, AddTwo.objects.count())
        self.assertEqual(result['sum'], 5  )
        self.assertEqual(result['occurrences'], 1)


class SquareDiffServiceTestCase(TestCase):
    def test_get_instance(self):
        self.assertEqual(0, SquareDiffs.objects.count())

        service = SquareDiffService([1])
        instance = service.get_instance()
        self.assertEqual(1, SquareDiffs.objects.count())
        self.assertEqual(1, instance.number)
        self.assertEqual(0, instance.value)
        self.assertEqual(0, instance.occurrences)

        # do it again -- should be isomorphic
        service = SquareDiffService([1])
        instance = service.get_instance()
        self.assertEqual(1, SquareDiffs.objects.count())
        self.assertEqual(1, instance.number)
        self.assertEqual(0, instance.value)
        self.assertEqual(0, instance.occurrences)

        service = SquareDiffService([10])
        instance = service.get_instance()
        self.assertEqual(2, SquareDiffs.objects.count())
        self.assertEqual(10, instance.number)
        self.assertEqual(2640, instance.value)
        self.assertEqual(0, instance.occurrences)

    def test_valid_input(self):
        self.assertEqual(0, SquareDiffs.objects.count())

        service = SquareDiffService([1])
        result = service.get_calculation()
        self.assertEqual(1, SquareDiffs.objects.count())
        self.assertEqual(1, result['number'])
        self.assertEqual(0, result['value'])
        self.assertEqual(1, result['occurrences'])

        # do it again -- should increment occurrences
        service = SquareDiffService([1])
        result = service.get_calculation()
        self.assertEqual(1, SquareDiffs.objects.count())
        self.assertEqual(1, result['number'])
        self.assertEqual(0, result['value'])
        self.assertEqual(2, result['occurrences'])

        service = SquareDiffService([10])
        result = service.get_calculation()
        self.assertEqual(2, SquareDiffs.objects.count())
        self.assertEqual(10, result['number'])
        self.assertEqual(2640, result['value'])
        self.assertEqual(1, result['occurrences'])


    def test_invalid_input(self):
        self.assertEqual(0, SquareDiffs.objects.count())

        service = SquareDiffService([0])
        with self.assertRaises(SquareDiffService.InputError):
            result = service.get_calculation()

        service = SquareDiffService([-10])
        with self.assertRaises(SquareDiffService.InputError):
            result = service.get_calculation()

        with self.assertRaises(SquareDiffService.InputError):
            service = SquareDiffService(['asdf'])

        self.assertEqual(0, SquareDiffs.objects.count())


class TripletServiceTestCase(TestCase):
    # assuming that if this works for AddTwo and for SquareDiff, it'll work for
    # Triplet too, so testing only at highest level
    def test_valid_input(self):
        self.assertEqual(0, Triplet.objects.count())

        service = TripletService([1, 2, 3])
        result = service.get_calculation()
        self.assertEqual(1, Triplet.objects.count())
        self.assertEqual(6, result['product'])
        self.assertEqual(False, result['is_pythagorean'])
        self.assertEqual(1, result['occurrences'])

        service = TripletService([3, 2, 1])
        result = service.get_calculation()
        self.assertEqual(1, Triplet.objects.count())
        self.assertEqual(6, result['product'])
        self.assertEqual(False, result['is_pythagorean'])
        self.assertEqual(2, result['occurrences'])

    def test_invalid_input(self):
        self.assertEqual(0, Triplet.objects.count())

        service = TripletService([-1, 2, 3])
        with self.assertRaises(TripletService.InputError):
            result = service.get_calculation()

        service = TripletService([2, 3])
        with self.assertRaises(TripletService.LookupError):
            result = service.get_calculation()


@freeze_time('2015-01-01 12:00:00')
@override_settings(ROOT_URLCONF='testing.urls')
class BaseCalculationViewTest(TestCase):
    # @TODO test misconfigured cases
    def test_get_input_values(self):
        view = AddTwoView()
        vals = view.get_input_values({}, { 'first': 1, 'second': 2, 'third': 3 })
        self.assertItemsEqual([1, 2], vals)

        with self.assertRaises(view.InputError):
            view.get_input_values({}, { 'first': 1, 'third': 3})

    def test_calculate(self):
        view = AddTwoView(calculator_class=AddTwoService)
        result = view.calculate({}, [1, 2])
        result.pop('id', None)

        self.assertEqual({ 'first': 1, 'second': 2, 'sum': 3, 'occurrences': 1}, result)

    def test_prep_response(self):
        view = AddTwoView()
        result = view.prep_response_data({}, {'hi': 'world', 'id': 'wtf'})
        self.assertEqual({ 'hi': 'world',
                           'datetime': datetime.datetime(2015, 1, 1, 12, 0, 0, 0, pytz.UTC)},
                         result)

    def test_valid_requests(self):
        resp = self.client.get(reverse('testing-add_two'),
                               { 'first': 1, 'second': 2 })
        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'first': 1,
                           'second': 2,
                           'sum': 3,
                           'occurrences': 1,
                           'datetime': '2015-01-01T12:00:00Z'},
                         data)

        with freeze_time('2015-01-01 12:30:00'):
            # second response should increment occurrences, change datetime
            resp = self.client.get(reverse('testing-add_two'),
                                   { 'first': 1, 'second': 2 })
        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'first': 1,
                           'second': 2,
                           'sum': 3,
                           'occurrences': 2,
                           'datetime': '2015-01-01T12:30:00Z'},
                         data)

        resp = self.client.get(reverse('testing-add_two'),
                               { 'first': 3, 'second': 2 })
        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'first': 2,
                           'second': 3,
                           'sum': 5,
                           'occurrences': 1,
                           'datetime': '2015-01-01T12:00:00Z'},
                         data)

    def test_bad_input(self):
        resp = self.client.get(reverse('testing-add_two'),
                               { 'first': 1 })
        self.assertEqual(400, resp.status_code)

        resp = self.client.get(reverse('testing-add_two'),
                               { 'first': 1, 'second': -2 })
        self.assertEqual(400, resp.status_code)


@freeze_time('2015-01-01 12:00:00')
class SquareDiffViewTest(TestCase):
    def test_valid_input(self):
        resp = self.client.get(reverse('calculations-square_diffs'),
                               { 'number': 10 })

        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'number': 10,
                           'value': 2640,
                           'occurrences': 1,
                           'datetime': '2015-01-01T12:00:00Z'},
                         data)

        with freeze_time('2015-01-01 12:30:00'):
            # second response should increment occurrences, change datetime
            resp = self.client.get(reverse('calculations-square_diffs'),
                                   { 'number': 10 })

        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'number': 10,
                           'value': 2640,
                           'occurrences': 2,
                           'datetime': '2015-01-01T12:30:00Z'},
                         data)

    def test_missing_field(self):
        resp = self.client.get(reverse('calculations-square_diffs'),
                               { 'wrong_number': 10 })

        self.assertEqual(400, resp.status_code)

    def test_invalid_input(self):
        resp = self.client.get(reverse('calculations-square_diffs'),
                               { 'number': -10 })

        self.assertEqual(400, resp.status_code)



@freeze_time('2015-01-01 12:00:00')
class TripletViewTest(TestCase):
    # Again, assuming this works; testing only simple cases
    def test_valid_input(self):
        resp = self.client.get(reverse('calculations-triplet'),
                               { 'a': 3, 'b': 2, 'c': 1 })

        self.assertEqual(200, resp.status_code)
        data = simplejson.loads(resp.content)
        self.assertEqual({ 'a': 1,
                           'b': 2,
                           'c': 3,
                           'product': 6,
                           'is_pythagorean': False,
                           'occurrences': 1,
                           'datetime': '2015-01-01T12:00:00Z'},
                         data)

    def test_invalid_input(self):
        resp = self.client.get(reverse('calculations-triplet'),
                               { 'a': 3, 'b': 2 })

        self.assertEqual(400, resp.status_code)
