#########################
Backstage Coding Exercise
#########################

********
Math API
********

Installation
============
::

	pip install -e ./
	./manage.py migrate

Usage
=====
::

	# ./manage.py runserver 0.0.0.0:8000
	
Retrieve calculations::
	
	wget http://localhost:8000/difference?number=10
	wget http://localhost:8000/triplet?a=1&b=2&c=3
	
Notes
=====

The first thing I actually did before starting this exercise was to make sure that all the numbers we'd be playing with could be handled by a standard 32-bit integer, since dealing with overflow would be an interesting twist to the challenge.  Wolfram Alpha quickly informed me that ``Sum(i^2, [i, 1, 100]) = 338350`` and ``Sum(i, [i, 1, 100])^2 = 25502500`` -- both of which are well within the range of even a signed 32-bit integer.

Also, there are shortcuts to those calculations.  Namely: 

* The sum of consecutive integers from 1 to n is ``n(n + 1)/2``
* The sum of the squares of integers from 1 to n is ``n(n+1)(2n+1)/6``

Optional Challenge
------------------

I could have addressed either optional challenge but chose the extensibility over the front-end one because it seemed like a more interesting engineering question.  I'd be happy to implement a JS-based front end if you'd like a demonstration of my Javascript skills.

Obviously this version is over-engineered for a service that merely calculates two numbers and returns their difference, but the intention is to permit the addition of new calculation types with a simple mostly-declarative syntax.  For instance, here's an AddThreeIsEven implementation::

	# models.py
	from calculations.models import BaseCalculationCache
	from django.db import models
	
	class AddThree(BaseCalculationCache):
		result_field = 'sum'
	
		first = models.PositiveIntegerField()
		second = models.PositiveIntegerField()
		third = models.PositiveIntegerField()
		is_even = models.BooleanField(blank=True)
		sum = models.BigIntegerField(blank=True)

		def calculate_result(self):
			self.sum = self.first + self.second + self.third
			self.is_even = not (self.sum % 2)

		class Meta:
			unique_together = ['first', 'second', 'third']


	# services.py
	from calculations.services import BaseCalculationService
	from models import AddThree
	
	class AddThreeService(BaseCalculationService):
		model = AddThree
		lookup_keys = ['first', 'second', 'third']


	# views.py
	from calculations.views import BaseCalculationView
	from services import AddThreeService
	
	class AddThreeView(BaseCalculationView):
		calculation_service = AddThreeService
		input_keys = ['first', 'second', 'third']
		

Currently the BaseCalculationService enforces that all input values are integers, but it could be modified reasonably easily to accept strings, floats, etc.

Storage
-------

Depending on the particular calculation, retrieving the result from a DB cache could be a gain, a loss, or wash in terms of computing time; but since we're storing, updating, and retrieving the request count anyway, we might as well cache the calculation as well.

I chose SQLite as the data store in this case for the sake of simplicity, but SQLite is limited in terms of everything from validation to handling concurrent requests.  In production I generally prefer MySQL+InnoDB or PostgreSQL for relational data storage.  A cache such as Redis or memcached might also be an option for this particular use case, but Django models are convenient, and SQLite is effortless.


Assumptions
-----------

Assumptions made:

* The requested difference in the squares / sums case is always the absolute value ofthe difference.  (Since it wasn't specified in which direction the difference should be taken.)  If that's not the case, that would be easy to change.
* either the request variable will always be sorted or the response variable name/values do not have to match those in the request exactly
* occurrences should be counted for the same input regardless of variable order --

For instance, if our request data for the Pythagorean endpoint is ``a=4&b=3&c=5``, we're sorting the integers before looking up the cached data, and returning them as::

	{ 
		"a": 3,
		"b": 4,
		"c": 5,
		"product": 60,
		"is_pythagorean_triple": true,
		"datetime": "2015-01-01T12:00:00Z",
		"occurrences": 1
	}
	
If the variable name correspondences had to be preserved, I'd probably: 

* configure the calculation service with a list of variable names that need to be preserved
* pass the original request data (with key/value correspondances) through to the calculation service, rather than values only
* have the service restore the variable names as an extra step before returning the data


If the correspondences had to be preserved and the occurrences should be calculated separately for different orderings of a/b/c, the easiest thing to do would be to avoid sorting the input data before doing the lookup, and simply filtering the request to retrieve the variables needed for lookup.