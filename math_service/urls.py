from django.conf.urls import include, url
from calculations import urls as calc_urls

urlpatterns = [
    url(r'', include(calc_urls)),
]
