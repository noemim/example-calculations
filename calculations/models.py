import logging
from django.core.validators import MinValueValidator
from django.db import models

logger = logging.getLogger('calculations')

class BaseCalculationCache(models.Model):
    result_field = 'result'

    occurrences = models.PositiveIntegerField(blank=True, default=0)

    def calculate_result(self):
        raise NotImplementedError()

    def has_result(self):
        return getattr(self, self.result_field) is not None

    def save(self, *args, **kwargs):
        if not self.has_result():
            self.calculate_result()
        self.full_clean()
        super(BaseCalculationCache, self).save(*args, **kwargs)

    def run(self):
        if not self.has_result():
            self.calculate_result()
        # @TODO: implement in raw SQL to avoid race conditions
        # UPDATE <table> SET request_count = request_count + 1 WHERE id=?
        self.occurrences += 1
        self.save()

    class Meta:
        abstract = True


class SquareDiffs(BaseCalculationCache):
    result_field = 'value'

    # Note: SQLite doesn't validate mins for positive integers, hence the
    # explicit validators
    number = models.PositiveIntegerField(unique=True, validators=[MinValueValidator(1),])
    value = models.PositiveIntegerField(blank=True)

    def calculate_result(self):
        # sum of consecutive integers from 1 to n is n(n + 1)/2
        sums = self.number * (self.number + 1) / 2
        square_sums = sums ** 2
        # sum of squares of integers from 1 to n is n(n+1)(2n+1)/6
        sum_squares = self.number * (self.number + 1) * (2 * self.number + 1) / 6
        self.value = abs(sum_squares - square_sums)


class Triplet(BaseCalculationCache):
    result_field = 'product'

    a = models.PositiveIntegerField(validators=[MinValueValidator(1),])
    b = models.PositiveIntegerField(validators=[MinValueValidator(1),])
    c = models.PositiveIntegerField(validators=[MinValueValidator(1),])
    product = models.PositiveIntegerField(blank=True)
    is_pythagorean = models.NullBooleanField(blank=True)

    def calculate_result(self):
        self.product = self.a * self.b * self.c
        self.is_pythagorean = (self.a**2 + self.b**2) == self.c**2

    class Meta:
        unique_together = ['a', 'b', 'c']
