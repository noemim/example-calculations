from calculations.views import SquareDiffsView, TripletView
from django.conf.urls import include, url

urlpatterns = [
    url(r'^difference$', SquareDiffsView.as_view(), name='calculations-square_diffs'),
    url(r'^triplet$', TripletView.as_view(), name='calculations-triplet'),
]
