import logging
from calculations.services import SquareDiffService, TripletService
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseServerError
from django.utils import timezone
from django.views.generic import View

logger = logging.getLogger('calculations')


class BaseCalculationView(View):
    http_method_names = ['get']
    calculation_service = None
    input_keys = []

    class ConfigError(Exception):
        pass

    class InputError(Exception):
        pass

    def dispatch(self, request, *args, **kwargs):
        try:
            self.calculator_class = self.get_calculation_service(request)
        except self.ConfigError, e:
            return HttpResponseServerError(u"Sorry, something went wrong internally!")

        return super(BaseCalculationView, self).dispatch(request, *args, **kwargs)

    def get_calculation_service(self, request):
        if self.calculation_service is None:
            raise self.ConfigError(u"No calculation service defined")
        return self.calculation_service

    def get_input_keys(self, request):
        if not len(self.input_keys):
            raise self.ConfigError(u"Input keys not specified")
        return self.input_keys

    def get_input_values(self, request, input):
        keys = self.get_input_keys(request)
        try:
            return [input[key] for key in keys]
        except KeyError:
            raise self.InputError(u"Missing required input field")

    def calculate(self, request, input_vals):
        calculator = self.calculator_class(input_vals)
        return calculator.get_calculation()

    def prep_response_data(self, request, response_data):
        response_data['datetime'] = timezone.now()
        if 'id' in response_data:
            del response_data['id']
        return response_data

    def get(self, request):
        input = request.GET

        try:
            input_vals = self.get_input_values(request, input)
        except self.InputError, e:
            # wouldn't necessarily usually trust error text to be safe to show
            # the user, but this is a toy project
            return HttpResponseBadRequest(str(e))
        except self.ConfigError, e:
            logger.error(e)
            return HttpResponseServerError(u"Sorry, something went wrong internally!")

        try:
            result = self.calculate(request, input_vals)
        except (self.calculator_class.InputError, self.calculator_class.LookupError), e:
            # likewise re: error text
            return HttpResponseBadRequest(str(e))

        result = self.prep_response_data(request, result)

        return JsonResponse(result)



class SquareDiffsView(BaseCalculationView):
    calculation_service = SquareDiffService
    input_keys = ['number']


class TripletView(BaseCalculationView):
    calculation_service = TripletService
    input_keys = ['a', 'b', 'c']
