from calculations.models import SquareDiffs, Triplet
from django.forms.models import model_to_dict
from django.core.exceptions import ValidationError


class BaseCalculationService(object):
    model = None
    lookup_keys = []

    class ConfigError(Exception):
        pass

    class LookupError(Exception):
        pass

    class UniquenessError(Exception):
        pass

    class InputError(Exception):
        pass


    def __init__(self, vals, *args, **kwargs):
        super(BaseCalculationService, self).__init__(*args, **kwargs)
        try:
            self.vals = [int(val) for val in vals]
        except ValueError:
            raise self.InputError(u"Invalid input.  Please ensure that all inputs are "
                                  u"natural numbers (positive integers)")

    def get_model_class(self):
        if self.model is None:
            raise self.ConfigError(u"Calculation service must define model")
        return self.model

    def get_lookup_keys(self):
        if not self.lookup_keys:
            raise self.ConfigError(u"At least one lookup key must be defined")
        return self.lookup_keys

    def get_ordered_vals(self):
        return sorted(self.vals)

    def get_lookup(self):
        lookup = {}
        keys = self.get_lookup_keys()
        vals = self.get_ordered_vals()

        if len(vals) != len(keys):
            raise self.LookupError(u"Incorrect number of lookup keys or request values")

        # map keys to values in order
        for index, key in enumerate(keys):
            lookup[key] = vals[index]

        return lookup

    def get_instance(self):
        lookup = self.get_lookup()
        try:
            instance, created = self.model.objects.get_or_create(**lookup)
            return instance
        except self.model.MultipleObjectsReturned:
            raise self.UniquenessError(u"Multiple results with same lookup keys. Check uniqueness indexing.")
        except ValidationError:
            raise self.InputError(u"Model could not be created with given input.  Please "
                                  u"ensure that all inputs are natural numbers (positive integers)")

    def get_calculation(self):
        instance = self.get_instance()
        instance.run()
        return model_to_dict(instance)


class SquareDiffService(BaseCalculationService):
    model = SquareDiffs
    lookup_keys = ['number']


class TripletService(BaseCalculationService):
    model = Triplet
    lookup_keys = ['a', 'b', 'c',]
