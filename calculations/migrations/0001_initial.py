# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SquareDiffs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('n', models.PositiveIntegerField(unique=True)),
                ('value', models.PositiveIntegerField()),
                ('occurrences', models.PositiveIntegerField(default=0, blank=True)),
            ],
        ),
    ]
