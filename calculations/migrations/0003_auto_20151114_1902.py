# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('calculations', '0002_auto_20151114_1829'),
    ]

    operations = [
        migrations.CreateModel(
            name='Triplet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('occurrences', models.PositiveIntegerField(default=0, blank=True)),
                ('a', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('b', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('c', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('product', models.PositiveIntegerField(blank=True)),
                ('is_pythagorean', models.NullBooleanField()),
            ],
        ),
        migrations.AlterField(
            model_name='squarediffs',
            name='value',
            field=models.PositiveIntegerField(blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='triplet',
            unique_together=set([('a', 'b', 'c')]),
        ),
    ]
