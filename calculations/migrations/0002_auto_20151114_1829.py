# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('calculations', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='squarediffs',
            name='n',
        ),
        migrations.AddField(
            model_name='squarediffs',
            name='number',
            field=models.PositiveIntegerField(default=0, unique=True, validators=[django.core.validators.MinValueValidator(1)]),
            preserve_default=False,
        ),
    ]
