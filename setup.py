#!/usr/bin/env python
import os
import sys

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

readme = open('README.rst').read()

setup(
    name='math_service',
    version='0.1',
    description='Math Service (Backstage Coding Exercise)',
    long_description=readme + '\n\n',
    author='Noemi Millman',
    author_email='noemi@triopter.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "django",
        "pytz",
        "simplejson",
        "freezegun",
    ],
    license="MIT",
    zip_safe=False,
)

